<?php
get_header();

 ?>
	<div class="feature-blocks row">
		
		<!-- BIG NEWS -->
		<div class="col-sm-6">
			<div class="featuredItem">
			<?php query_posts( 'posts_per_page=1' ); 
			while(have_posts()) : the_post();
				echo homepage_content_block();
			endwhile; wp_reset_query(); ?>
			</div>
		</div>
		
		<!-- FIRST FOUR -->
		<div class="col-sm-6">
			<div class="row">
			<?php query_posts('posts_per_page=4&offset=1');
				while(have_posts()) : the_post(); ?>
				<div class="col-xs-6">
					<?php echo homepage_content_block(); ?>
				</div>
				<?php endwhile; wp_reset_query(); ?>
			</div>
		</div>
		
		<!-- SECOND FOUR -->
		<div class="col-sm-12 hidden-xs" id="extraBlocks">
			<div class="row">
			<?php query_posts('posts_per_page=8&offset=5');
				while(have_posts()) : the_post(); ?>
				<div class="col-xs-3">
					<?php echo homepage_content_block(); ?>
				</div>
				<div class="col-xs-3">
					<?php echo homepage_content_block(); ?>
				</div>
				<div class="col-xs-3">
					<?php echo homepage_content_block(); ?>
				</div>
				<div class="col-xs-3">
					<?php echo homepage_content_block(); ?>
				</div>
				<div class="col-xs-3">
					<?php echo homepage_content_block(); ?>
				</div>
				<?php endwhile; wp_reset_query(); ?>
			</div>
		</div>
		
		
		
		
	</div><!-- .feature-blocks -->
	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'content', 'homepage' ); ?>
	<?php endwhile; // end of the loop. ?>

<?php get_sidebar('homepage'); ?>
<?php get_footer(); ?>
