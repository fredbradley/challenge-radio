<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package challengeradio
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="page-header">
		<h1 class="page-title"><?php the_title(); ?></h1>
	</header><!-- .entry-header -->

	<div class="entry-content row">
		<?php if (get_the_ID() == 140) {
			echo "<div class=\"col-sm-12\">";
			the_content();
			echo "</div>";
		} else { ?>
		<div class="col-sm-4">
				<?php the_post_thumbnail('newsimg', array('class' => 'img-responsive img-rounded pull-right')); ?>
		</div>
		<div class="col-sm-8">
				<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'challengeradio' ),
				'after'  => '</div>',
			) );
		?>
		</div>
		<?php } ?>

	</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', 'challengeradio' ), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->
