<?php
	function file_get_contents_curl($url) {
		$ch = curl_init();


		curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
	
		$data = curl_exec($ch);
		curl_close($ch);
	
	return $data;
	}
	function sra_json_decode_curl($json) {
	$ch = curl_init(); // open curl session

	// set curl options
	curl_setopt($ch, CURLOPT_URL, $json);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);    
	$data = curl_exec($ch); // execute curl session
	curl_close($ch); // close curl session

//	$data = str_replace( 'jsonFlickrApi(', '', $data );
//	$data = substr( $data, 0, strlen( $data ) - 1 ); //strip out last paren

	return json_decode($data);
}


$epg = '[  
  
   {  
      "id":78,
      "start":1413279000,
      "end":1413288000,
      "title":"Ken Bruce",
      "description":"Ken brings his BBC Radio 2 show to Challenge Radio...",
      "image":"http:\/\/www.challengeradio.co.uk\/wp-content\/uploads\/2014\/10\/Ken-Bruce-150x150.jpg"
   },
   {  
      "id":80,
      "start":1413288000,
      "end":1413291600,
      "title":"Stuart & Clint",
      "description":"6music & XFM Manchester combine in the world\'s biggest Crystal Maze ball replica. ",
      "image":"http:\/\/www.challengeradio.co.uk\/wp-content\/uploads\/2014\/10\/mac-boon-150x150.jpg"
   },
   {  
      "id":82,
      "start":1413291600,
      "end":1413295200,
      "title":"Anneka & Adam",
      "description":"Challenge Radio\'s mother & naughty nephew join forces!",
      "image":"http:\/\/www.challengeradio.co.uk\/wp-content\/uploads\/2014\/10\/adamannekasquare-150x150.jpg"
   },
   {  
      "id":84,
      "start":1413295200,
      "end":1413298800,
      "title":"Max & Chris",
      "description":"Like sport? Max and Chris do too!",
      "image":"http:\/\/www.challengeradio.co.uk\/wp-content\/uploads\/2014\/10\/rushdenwarb-150x150.jpg"
   },
   {  
      "id":86,
      "start":1413298800,
      "end":1413302400,
      "title":"Raj & Pablo",
      "description":"The best in Asian music.",
      "image":"http:\/\/www.challengeradio.co.uk\/wp-content\/uploads\/2014\/10\/RajandPablo1-150x150.jpg"
   },
   {  
      "id":88,
      "start":1413302400,
      "end":1413306000,
      "title":"Bex ",
      "description":"Relive your childhood, with Bex Lindsay from Fun Kids!",
      "image":"http:\/\/www.challengeradio.co.uk\/wp-content\/uploads\/2014\/10\/Bex-Headshot-150x150.jpg"
   },
   {  
      "id":90,
      "start":1413306000,
      "end":1413309600,
      "title":"Fi, Jane & Joel",
      "description":"The Charlie\'s Angels of Challenge Radio. ",
      "image":"http:\/\/www.challengeradio.co.uk\/wp-content\/uploads\/2014\/10\/joelfijane-150x150.jpg"
   },
   {  
      "id":92,
      "start":1413309600,
      "end":1413313200,
      "title":"Ben Burrell",
      "description":"The Official Biggest Selling Singles of the Millennium, compiled by the Official Charts Company! Who do you think it will be? Tweet us @ChallengeRadio ",
      "image":"http:\/\/www.challengeradio.co.uk\/wp-content\/uploads\/2014\/10\/BenBurrellchart1-150x150.jpg"
   },
   {  
      "id":94,
      "start":1413313200,
      "end":1413316800,
      "title":"Glen & Emma",
      "description":"Bringing you the very best of student radio life, without the hangover. ",
      "image":"http:\/\/www.challengeradio.co.uk\/wp-content\/uploads\/2014\/10\/Glen-and-Emma-150x150.jpg"
   }
]';

//$json = file_get_contents_curl("http://www.challengeradio.co.uk/epg");
//$json = json_decode($epg);
$json = sra_json_decode_curl("http://www.challengeradio.co.uk/epg/");


	foreach ($json as $show) {
		if (time() > $show->start && time() < $show->end) {
	?>
<div class="content-box">
	
	<div class="image-container pull-left">
		<a href="<?php echo get_permalink($show->id); ?>"><img src="<?php echo $show->image; ?>" class="img-rounded" /></a>
	</div>
	<h4 class=""><?php echo $show->title; ?></h4>
	<div class="show-blurb">
		<p><?php echo $show->description; ?></p>
		<p><a href="<?php echo get_permalink($show->id); ?>">Find out more...</a></p>
	</div>
</div>
<?php if (is_user_logged_in()):
echo date("g:i a");
endif; ?>


<?php
$done = true; }

}
if ($done !== true): ?>

<div class="content-box">
	
	<div class="image-container pull-left">

		<a href="http://www.challengeradio.co.uk/2014/10/12-hours-of-broadcasting-complete"><img src="http://www.challengeradio.co.uk/wp-content/uploads/2014/10/Bz_1b2WCEAID_T7-150x150.jpg?ac8b0" class="img-rounded" /></a>
	</div>
	<h4 class="">THANKYOU</h4>
	<div class="show-blurb">
		<p>Thank you for listening to Challenge Radio, the first time we have done a live pop up radio station at Radio Festival!</p>
		<p><i class="fa fa-fw fa-headphones"></i> <a href="http://www.challengeradio.co.uk/2014/10/12-hours-of-broadcasting-complete">Thank you</a> | <i class="fa fa-fw fa-video-camera"></i> <a href="http://www.challengeradio.co.uk/2014/10/watch-the-best-bits-from-challenge-radio/">Best Bits</a>
		
	</div>
</div>
<?php endif;