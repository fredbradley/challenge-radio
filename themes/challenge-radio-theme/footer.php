<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package challengeradio
 */
?>
			</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->
<div id="flare-footer">&nbsp;</div>
<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12">
				<h3 class="widget-title">Ways to Listen</h3>
				<p>You can listen to Challenge Radio on 87.9 FM &amp; DAB in the Salford area of Manchester. <small>(You might have to retune your DAB radio on Tuesday morning!)</small>. We are also on the <a href="javascript:popUpListen();">Radioplayer</a> so you listen online too!</p>
				<?php wp_nav_menu(
				array(
					'theme_location' => 'foot_shows',
					'container_class' => 'footer-nav',
					'menu_class' => 'nav nav-pills nav-stacked',
					'fallback_cb' => '__return_false',
					'menu_id' => 'footer-nav',
					'container_id' => 'footer-nav',
					'walker' => new wp_bootstrap_navwalker()
				)
			); ?>
			</div>
			
			
			
			<div class="col-md-3 col-sm-6">
				<h3 class="widget-title">About Us</h3>
				<?php wp_nav_menu(
				array(
					'theme_location' => 'foot_watch',
					'container_class' => 'footer-nav',
					'menu_class' => 'nav nav-pills nav-stacked',
					'menu_id' => 'footer-nav',
					'container_id' => 'footer-nav',
					'fallback_cb' => '__return_false',
					'walker' => new wp_bootstrap_navwalker()
				)
			); ?>
			</div>
			
			<div class="col-md-3 col-sm-6">
				<img src="<?php echo get_stylesheet_directory_uri() .'/images/radfest_logo.png'; ?>" class="img-responsive" />
			</div>
			
			
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="site-footer-inner col-sm-12">

				<div class="site-info pull-right">
					
				</div><!-- close .site-info -->

			</div>
		</div>
	</div><!-- close .container -->
</footer><!-- close #colophon -->
<div id="search">
    <button type="button" class="close">×</button>
    <form>
        <input type="search" value="" placeholder="search..." />
        <button type="submit" class="btn btn-radfest btn-lg">Search</button>
    </form>
</div>
<script type="text/javascript" async src="//platform.twitter.com/widgets.js"></script>
<?php wp_footer(); ?>
<script>
jQuery( document ).ready(function($) {
  // Handler for .ready() called.
	var nt_example2 = $('#nt-example2').newsTicker({
		row_height: 30,
		max_rows: 1,
	//	speed: 300,
		duration: 8000,
		prevButton: $('#nt-example2-prev'),
		nextButton: $('#nt-example2-next'),
		hasMoved: function() {
	    	$('#nt-example2-infos-container').fadeOut(200, function(){
	        	$('#nt-example2-infos .infos-hour').text($('#nt-example2 li:first span').text());
	        	$('#nt-example2-infos .infos-text').text($('#nt-example2 li:first').data('infos'));
	        	$(this).fadeIn(400);
	        });
	    },
	    pause: function() {
	    	$('#nt-example2 li i').removeClass('fa-play').addClass('fa-pause');
	    },
	    unpause: function() {
	    	$('#nt-example2 li i').removeClass('fa-pause').addClass('fa-play');
	    }
	});
	$('#nt-example2-infos').hover(function() {
	    nt_example2.newsTicker('pause');
	}, function() {
	    nt_example2.newsTicker('unpause');
	});

	jQuery('#newsTicker ul').newsTicker({
	    row_height: 20,
	    max_rows: 1,
	    duration: 4000,
	//    prevButton: jQuery('#nt-example1-prev'),
	//    nextButton: jQuery('#nt-example1-next')
	});

	jQuery('#emapTweet').newsTicker({
		row_height:100,
		max_rows:1,
		duration:4000,
	});
	
	jQuery('#emapTweet').append("<li>Here is a test</li>");

});
</script>
<script type="text/javascript">

var contactClone = jQuery("#contactform").clone(); // Do this on $(document).ready(function() { ... })
jQuery("#contactform").replaceWith(contactClone); 
//$("#some_div").html("Yeah all good mate!"); // Change the content temporarily

// Use this command if you want to keep divClone as a copy of "#some_div"
//jQuery("#contactform").replaceWith(contactClone.clone()); // Restore element with a copy of divClone

// Any changes to "#some_div" after this point will affect the value of divClone
jQuery("#contactform").replaceWith(contactClone); 


// Attach a submit handler to the form
// variable to hold request
var request;
// bind to the submit event of our form
jQuery("#contactform").submit(function(event){
var contactClone = jQuery("#contactform").clone(); // Do this on $(document).ready(function() { ... })
    // abort any pending request
    if (request) {
        request.abort();
    }
    // setup some local variables
    var $form = jQuery(this);
    // let's select and cache all the fields
    var $inputs = $form.find("input, select, button, textarea");
    // serialize the data in the form
    var serializedData = $form.serialize();

    // let's disable the inputs for the duration of the ajax request
    // Note: we disable elements AFTER the form data has been serialized.
    // Disabled form elements will not be serialized.
    $inputs.prop("disabled", true);

    // fire off the request to /form.php
    request = jQuery.ajax({
        url: "/wp-content/themes/challenge-radio-theme/contact-form-function.php",
        type: "post",
        data: serializedData
    });

    // callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        // log a message to the console
        console.log("Hooray, it worked!");
        jQuery("#contactform .results").html("<div class=\"alert alert-success\">Thankyou. Your email will be received in the studio in moments!</div>");

    });

    // callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // log the error to the console
        console.error(
            "The following error occured: "+
            textStatus, errorThrown
        );
    });

    // callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function () {
        // reenable the inputs
        $inputs.prop("disabled", false);
    });

    // prevent default posting of form
    event.preventDefault();
});


</script>
<script language="JavaScript">
		function popUpListen() {
		props=window.open('http://radioplayer.challengeradio.co.uk', 'popradioplayer', 'toolbars=0, scrollbars=0, location=0, statusbars=0, menubars=0, resizable=0, width=385, height=665, left=0, top=0, title=PlayRadio Console'); }
	</script>

</body>
</html>