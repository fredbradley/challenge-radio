<?php
/**
 * @package challengeradio
 */
?>
<script>
jQuery( document ).ready(function($) {
	//$('a.scroller').click(function(){
	    $('html, body').animate({
	        scrollTop: $( $("#content") ).offset().top
	    }, 500);
	    return false;
	//});
	$('#start-button').click(function() {
		$(this).fadeOut(500);
	})
});

</script>
<article id="post-<?php the_ID(); ?>" <?php post_class('row'); ?>>
	<header class="page-header">
		<h1 class="page-title"><?php the_title(); ?></h1>

		<div class="entry-meta">
			<?php challengeradio_posted_on(); ?>
			<?php edit_post_link('<i class="fa fa-fw fa-edit"></i>', '<span>', '</span>'); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content col-md-12">
		<div class="row">
			<div class="col-sm-4">
				<?php the_post_thumbnail('newsimg', array("class" => 'img-responsive img-rounded')); ?>
			</div>
			<div class="thecontent col-sm-8">
				<?php if (get_post_type() == "programmes") {
				//	if (empty($post->post_content)):
						echo "<p>".get_post_meta(get_the_ID(), 'challenge_meta_short_description', true)."</p>";
				//	endif;
				}
				the_content(); ?>
			</div>
		</div>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'challengeradio' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<div class="clear clearfix">&nbsp;</div>
	<footer class="entry-meta">
		<span class="the_tags">
		<?php 
		$posttags = get_the_tags();
				if ($posttags) {
					foreach($posttags as $tag) {
						$link = "/tag/".$tag->slug;
						echo "<a class=\"winegum orange\" href=\"".$link."\"><i class=\"fa fa-fw fa-tag\"></i>".$tag->name."</a> "; 
					}
				} ?>
		</span>
		<?php edit_post_link( __( 'Edit', 'challengeradio' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-## -->
