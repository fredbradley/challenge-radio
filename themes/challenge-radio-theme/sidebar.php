<?php
/**
 * The sidebar containing the main widget area
 *
 * @package challengeradio
 */
if (do_holdingpage_redirect() === false): 
?>

	</div><!-- close .main-content-inner -->

	<div class="sidebar col-sm-12 col-md-4">

		<?php // add the class "panel" below here to wrap the sidebar in Bootstrap style ;) ?>
		<div class="sidebar-padder">
			<aside id="video_highlight" class="widget widget_contact winegum pink">
				<h3 class="widget-title">Highlights</h3>

				<?php 
					if (is_front_page()) {
						$autoplay = "autoplay=\"on\""; 
					} else {
						$autoplay = "";
					}
					echo do_shortcode('[video '.$autoplay.' poster="http://www.challengeradio.co.uk/wp-content/uploads/2015/10/videoposter.jpg" mp4="http://www.challengeradio.co.uk/wp-content/uploads/2015/10/One-Year-On.mp4"][/video]'); ?>
			</aside>
			<?php /*
			<aside id="contactus" class="widget widget_contact winegum pink">
				<h3 class="widget-title">Email the Studio</h3>
				<form method="post" id="contactform" action="">
					<div class="results">&nbsp;</div>
					<div class="form-group">
						<input type="text" placeholder="Your Name..." class="form-control" name="name" />
					</div>
					<div class="form-group">
						<textarea class="form-control" placeholder="Your Message to the studio" name="message"></textarea>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-radfest">Send</button>
					</div>
				</form>
			</aside>*/ ?>
			
			<?php do_action( 'before_sidebar' ); ?>
			<?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>

				<aside id="search" class="widget widget_search">
					<?php get_search_form(); ?>
				</aside>

				<aside id="archives" class="widget widget_archive">
					<h3 class="widget-title"><?php _e( 'Archives', 'challengeradio' ); ?></h3>
					<ul>
						<?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
					</ul>
				</aside>

				<aside id="meta" class="widget widget_meta">
					<h3 class="widget-title"><?php _e( 'Meta', 'challengeradio' ); ?></h3>
					<ul>
						<?php wp_register(); ?>
						<li><?php wp_loginout(); ?></li>
						<?php wp_meta(); ?>
					</ul>
				</aside>

			<?php endif; ?>

		</div><!-- close .sidebar-padder -->
		

		
<?php endif;