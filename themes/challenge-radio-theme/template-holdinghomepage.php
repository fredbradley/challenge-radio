<?php
/**
 * Template Name: Holding Homepage
 * The template for displaying the homepage!
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package challengeradio
 */
	get_header('holdingpage');

 ?>
 		<div class="row">
	 		<div class="text-center">
		 		<a href="#content" class="scroller btn btn-lg btn-radfest" id="start-button">Start</a>
		 	</div>
			<div class="col-md-6">
				<h3 class="widget-title">Latest Tweets</h3>
				<div id="twitterTickerContainer" class="winegum orange">
					<ul class="list-unstyled" id="emapTweet"></ul>
				</div>
				<!--<div id="newsTicker" class="winegum orange">
					<ul class="list-unstyled">
						<?php echo $ticker; ?>
						<li><div class="twitter"><i class="fa fa-fw fa-twitter"></i>7 Here is a tweet!</div></li>
						<li><div class="facebook"><i class="fa fa-fw fa-facebook"></i>8 Here is a facebook post!</div></li>
						<li><div><i class="fa fa-fw fa-star"></i>9 Here is something different!</div></li>
					</ul>
				</div> -->
			</div>
			<div class="col-md-6">
				<h3 class="widget-title">Connect with us</h3>
				<div class="winegum orange">
					<div class="social-links-container text-center" style="margin:10px;height:80px;">
						<ul class="list-inline">
                            <li>
                                <a target="_blank" href="https://twitter.com/intent/user?screen_name=ChallengeRadio" class="btn-outline"><i class="fa fa-fw fa-twitter"></i> @ChallengeRadio</a>
                            </li>
                        </ul>
					</div>
				</div>
			</div>
			
			<div class="site-header-inner col-sm-12">

				<?php $header_image = get_header_image();
				if ( ! empty( $header_image ) ) { ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="">
					</a>
				<?php } // end if ( ! empty( $header_image ) ) ?>


				<div class="site-branding sr-only">
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<h4 class="site-description"><?php bloginfo( 'description' ); ?></h4>
				</div>

			</div>
		</div>

 
 
<div class="main-content">
	<div class="constrainer">
		<h3 class="widget-title hidden-md hidden-lg">Latest News</h3>
	<div class="feature-blocks row">
		
		
		
		<!-- FIRST FOUR -->
		<div class="col-sm-12">
			<div class="row">
			<?php
			 	$args = array(
			 		'posts_per_page' => 4,
			 	);
			 	$my_query = new WP_Query($args);
			 	$i=0;
				while($my_query->have_posts()) : $my_query->the_post();
//				echo $post->count;
			?>
				<div class="col-xs-6 col-sm-3 col-md-3">
					<?php echo homepage_content_block(); ?>
				</div>
				<?php $i++; ?>
				<?php endwhile; wp_reset_query(); ?>
				
			</div>
		</div>
		
		<!-- FIRST FOUR -->
		<div class="col-sm-12">
			<div class="row">
			<?php
			 	$args = array(
			 		'posts_per_page' => 4,
			 		'offset' => 4
			 	);
			 	$my_query = new WP_Query($args);
			 	$i=0;
				while($my_query->have_posts()) : $my_query->the_post();
//				echo $post->count;
			?>
				<div class="col-xs-6 col-sm-3">
					<?php echo homepage_content_block(); ?>
				</div>
				<?php $i++; ?>
				<?php endwhile; wp_reset_query(); ?>
				
			</div>
				
		</div>
		
	</div><!-- .feature-blocks -->
	</div>
</div>
 <?php if (strlen(get_the_content()) > 100 ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<h3 class="widget-title"><?php the_title(); ?></h3>
		<div class="well well-radfest text-justify">
		<?php get_template_part( 'content', 'homepage' ); ?>
		</div>
	<?php endwhile; // end of the loop. ?>
<?php endif; ?>
<?php 
	
	
	get_footer('holdingpage');

?>