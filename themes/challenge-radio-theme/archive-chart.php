<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package challengeradio
 */

get_header(); ?>

	<?php // add the class "panel" below here to wrap the content-padder in Bootstrap style ;) ?>
	<div class="content-padder">
<header class="page-header">
				
				<?php
					// Show an optional term description.
					$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );
					endif;
				?>
			</header><!-- .page-header -->
			<div class="entry-summary">
				<p>Join Ben Burrell as he counts down the top 10 Official Biggest Selling Singles of the Millennium, compiled by The Official Charts Company</p>
			</div>
			<hr />
		<?php if ( have_posts() ) : ?>

			

			<?php /* Start the Loop */ 
			
			$args = array(
				'posts_per_page' => -1,
				'post_type' => 'chart',
				'orderby' => 'meta_value_num',
				'order' => 'ASC',
				'meta_key' => 'challenge_meta_number'
			);
			query_posts($args);
			?>
			<?php while ( have_posts() ) : the_post(); 

// Want to wrap for example the post content in blog listings with a thin outline in Bootstrap style?
// Just add the class "panel" to the article tag here that starts below.
// Simply replace post_class() with post_class('panel') and check your site!
// Remember to do this for all content templates you want to have this,
// for example content-single.php for the post single view. ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('row'); ?>>
	<div class="col-md-3 col-sm-4">
			<?php the_post_thumbnail('newsimg', array('class' => 'img-responsive img-rounded')); ?>
	</div>
	<div class="col-md-9 col-sm-8">
		
	<header class="page-header">
		<h3 class="page-title">
			<?php the_title(); ?>
		</h3>

		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php challengeradio_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php if ( is_search() || is_archive() ) : // Only display Excerpts for Search and Archive Pages ?>
	<div class="entry-summary">
				<p class="winegum orange"><i class="fa fa-fw fa-headphones"></i> #<?php echo get_post_meta(get_the_ID(), 'challenge_meta_number', true); ?></p>

		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
			<p><i class="fa fa-fw fa-headphones"></i><?php echo get_post_meta(get_the_ID(), 'challenge_meta_number', true); ?></p>

		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'challengeradio' ) ); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'challengeradio' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<?php endif; ?>

	<footer class="entry-meta">
		<?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search ?>
			<?php
				/* translators: used between list items, there is a space after the comma */
				$categories_list = get_the_category_list( __( ', ', 'challengeradio' ) );
				if ( $categories_list && challengeradio_categorized_blog() ) :
			?>
			<span class="cat-links">
				<?php printf( __( 'Posted in %1$s', 'challengeradio' ), $categories_list ); ?>
			</span>
			<?php endif; // End if categories ?>

			<?php
				/* translators: used between list items, there is a space after the comma */
				$tags_list = get_the_tag_list( '', __( ', ', 'challengeradio' ) );
				if ( $tags_list ) :
			?>
			<span class="tags-links">
				<?php printf( __( 'Tagged %1$s', 'challengeradio' ), $tags_list ); ?>
			</span>
			<?php endif; // End if $tags_list ?>
		<?php endif; // End if 'post' == get_post_type() ?>

		

	</footer><!-- .entry-meta -->
	</div>
</article><!-- #post-## -->


			<?php endwhile; ?>

			<?php challengeradio_content_nav( 'nav-below' ); ?>

		<?php else : ?>

			<?php //get_template_part( 'no-results', 'archive' ); ?>

		<?php endif; ?>

	</div><!-- .content-padder -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
<?php 

