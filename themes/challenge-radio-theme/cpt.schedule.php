<?php
if ( ! function_exists('schedule') ) {

// Register Custom Post Type
function schedule() {

	$labels = array(
		'name'                => _x( 'Programmes', 'Post Type General Name', 'challengeradio' ),
		'singular_name'       => _x( 'Programme', 'Post Type Singular Name', 'challengeradio' ),
		'menu_name'           => __( 'Schedule', 'challengeradio' ),
		'parent_item_colon'   => __( 'Parent Item:', 'challengeradio' ),
		'all_items'           => __( 'All Items', 'challengeradio' ),
		'view_item'           => __( 'View Item', 'challengeradio' ),
		'add_new_item'        => __( 'Add New Item', 'challengeradio' ),
		'add_new'             => __( 'Add New', 'challengeradio' ),
		'edit_item'           => __( 'Edit Item', 'challengeradio' ),
		'update_item'         => __( 'Update Item', 'challengeradio' ),
		'search_items'        => __( 'Search Item', 'challengeradio' ),
		'not_found'           => __( 'Not found', 'challengeradio' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'challengeradio' ),
	);
	$args = array(
		'label'               => __( 'programmes', 'challengeradio' ),
		'description'         => __( 'Programmes', 'challengeradio' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'comments', 'trackbacks', 'revisions','thumbnail' ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'programmes', $args );

}

// Hook into the 'init' action
add_action( 'init', 'schedule', 0 );

}



