<?php
if (do_holdingpage_redirect()):
	get_header('holdingpage');
else:

	date_default_timezone_set("Europe/London");

/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package challengeradio
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> xmlns:og='http://opengraphprotocol.org/schema/'
 xmlns:fb='http://www.facebook.com/2008/fbml' prefix="og: http://ogp.me/ns#">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="icon" href="<?php echo get_template_directory_uri().'/favicon.ico'; ?>" type="image/x-icon" />

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
<link rel="author" href="https://plus.google.com/110474516290880858606" /> <!-- https://plus.google.com/109314841734487245346/about -->
<link rel="publisher" href="https://plus.google.com/110474516290880858606" />

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44652794-3', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body <?php body_class(); ?>>

<?php
	$items = array(
		array(
			'class' => 'twitter',
			'content' => "1 My wonderful tweet",
		),
		array(
			'class' => 'facebook',
			'content' => '2 My Facebook is awesome!'
		),
		array(
			"content" => "3 Here is a star, shining bright!"
		),
		array(
			'class' => 'twitter',
			'content' => "4 My wonderful tweet",
		),
		array(
			'class' => 'facebook',
			'content' => '5 My Facebook is awesome!'
		),
		array(
			"content" => "6 Here is a star, shining bright!"
		)
	);


$ticker = newsTicker($items);
?>
	<?php do_action( 'before' ); ?>
	<nav class="global-navigation">
		<div class="navbar navbar-inverse navbar-static-top">
			<div class="navbar-header">
				<!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#global-nav">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

				<!-- Your site title as branding in the menu -->
				<a class="navbar-brand hidden-sm" href="http://www.radioacademy.org/" target="_blank" title="The Radio Academy">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/RadAcadLogo.png" alt="">
					<span class="sr-only">The Radio Academy</span>
				</a>
			</div>

				<!-- The WordPress Menu goes here -->
			<?php wp_nav_menu(
				array(
					'theme_location' => 'global',
					'container_class' => 'collapse navbar-collapse navbar-responsive-collapse',
					'menu_class' => 'nav navbar-nav navbar-right',
					'fallback_cb' => '',
					'menu_id' => 'main-menu',
					'container_id' => 'global-nav',
					'walker' => new wp_bootstrap_navwalker()
				)
			); ?>

		</div><!-- .navbar -->
	</nav><!-- .site-navigation -->

<header id="masthead" class="site-header" role="banner">
	<div class="container">
		<div class="row visible-xs">
			
			<div class="col-xs-6">
				<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
					<h1 class="sr-only"><?php bloginfo('name'); ?></h1>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ChallengeRadioLogoTransparent-WhiteText.png" class="img-responsive" />
				</a>
			</div>
			<div id="mobile-buttons">
			<div class="col-xs-6 text-center">
				<div class="small-title">
					<a class="no-underline" href="http://www.challengeradio.co.uk/2014/10/12-hours-of-broadcasting-complete"><h3 class="widget-title text-center winegum pink">Listen</h3></a>
				</div>
				<div class="small-title">
					<a class="no-underline" href="http://www.challengeradio.co.uk/2014/10/watch-the-best-bits-from-challenge-radio/"><h3 class="widget-title text-center winegum pink">Watch</h3></a>
				</div>
			</div>
			</div>
			
		</div>
	
		<div class="row hidden-xs">
			<div class="col-md-3 col-sm-5" id="logo_container_lg">
				<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
					<h1 class="sr-only"><?php bloginfo('name'); ?></h1>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ChallengeRadioLogoTransparent-WhiteText.png" class="img-responsive" />
				</a>
			</div>
			
			<div class="col-md-6 col-md-offset-3 col-sm-7">
				<div id="nowbox" class="winegum orange nowbox">
					<?php get_template_part('element', 'nowbox'); ?>
				
					<!--<ul class="list-unstyled" id="emapTweet"></ul>-->
				</div>
				<div id="smobile-buttons">
			<!--	<div class="visible-xs visible-sm">
					<a class="no-underline" href="javascript:popUpListen();"><h3 class="widget-title text-center winegum pink">Listen</h3></a>
				</div> -->
				<div class="row text-center">
					<div class="small-title col-sm-6">
						<a class="no-underline" href="http://www.challengeradio.co.uk/2014/10/12-hours-of-broadcasting-complete">
							<h3 class="widget-title text-center winegum pink">Listen</h3>
						</a>
					</div>
					<div class="small-title col-sm-6">
						<a class="no-underline" href="http://www.challengeradio.co.uk/2014/10/watch-the-best-bits-from-challenge-radio/">
							<h3 class="widget-title text-center winegum pink">Watch</h3>
						</a>
					</div>
				</div>
				</div>
				<div class="clear clearfix">&nbsp;</div>
				<!--<div id="newsTicker" class="winegum blue">
					<ul class="list-unstyled">
						<?php echo $ticker; ?>
						<li><div class="twitter"><i class="fa fa-fw fa-twitter"></i>7 Here is a tweet!</div></li>
						<li><div class="facebook"><i class="fa fa-fw fa-facebook"></i>8 Here is a facebook post!</div></li>
						<li><div><i class="fa fa-fw fa-star"></i>9 Here is something different!</div></li>
					</ul>
				</div>-->
			</div>
			
			<div class="site-header-inner col-sm-12">

				<?php $header_image = get_header_image();
				if ( ! empty( $header_image ) ) { ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="">
					</a>
				<?php } // end if ( ! empty( $header_image ) ) ?>


				<div class="site-branding sr-only">
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<h4 class="site-description"><?php bloginfo( 'description' ); ?></h4>
				</div>

			</div>
		</div>
	</div><!-- .container -->
</header><!-- #masthead -->

<nav class="site-navigation">
	<div class="container">
		<div class="row">
			<div class="site-navigation-inner">
				<div class="navbar navbar-default">
					<div class="navbar-header">
					<!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#site-nav">
						<span class="sr-only">Toggle navigation</span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					</button>

					<!-- Your site title as branding in the menu -->
					
				  </div>

				<!-- The WordPress Menu goes here -->
				<div id="site-nav" class="collapse navbar-collapse navbar-responsive-collapse">
			<?php wp_nav_menu(
				array(
					'theme_location' => 'primary',
					'container_class' => '',
					'menu_class' => 'nav navbar-nav',
					'fallback_cb' => '',
					'menu_id' => 'main-menu',
					'container_id' => '',
					'depth' => 0,
					'walker' => new wp_bootstrap_navwalker()
				)
			); ?>
				<ul class="nav navbar-nav navbar-right hidden-xs">
                    <li><a href="#search"><i class="fa fa-fw fa-search"></i><span class="sr-only">Search</span></a></li>
                </ul>
				</div>
				</div><!-- .navbar -->
			</div>
		</div>
	</div><!-- .container -->
</nav><!-- .site-navigation -->

<div class="main-content">
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner col-sm-12 col-md-8">
<?php endif;
