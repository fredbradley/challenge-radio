<?php

class DownloadAppsWidget extends WP_Widget
{
  function DownloadAppsWidget()
  {
    $widget_ops = array('classname' => 'DownloadAppsWidget', 'description' => 'Displays the buttons to download the apps' );
    $this->WP_Widget('DownloadAppsWidget', 'Download Apps', $widget_ops);
  }
 
  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
    $title = $instance['title'];
?>
  <p><label for="<?php echo $this->get_field_id('title'); ?>">Title: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></label></p>
<?php
  }
 
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
    $instance['title'] = $new_instance['title'];
    return $instance;
  }
 
  function widget($args, $instance)
  {
    extract($args, EXTR_SKIP);
 
    echo $before_widget;
    $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
 
    if (!empty($title))
      echo $before_title . $title . $after_title;;
 
    // WIDGET CODE GOES HERE
    ?>
    <a href="<?php echo site_url('/apps'); ?>">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/Apps.png" class="img-responsive" />
	</a>
	<?php
    echo $after_widget;
  }
 
}
add_action( 'widgets_init', create_function('', 'return register_widget("DownloadAppsWidget");') );?>
