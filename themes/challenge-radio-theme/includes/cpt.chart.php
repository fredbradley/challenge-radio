<?php
// Register Custom Post Type
function chart() {

	$labels = array(
		'name'                => _x( 'Charts', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Chart', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Chart', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'All Items', 'text_domain' ),
		'view_item'           => __( 'View Item', 'text_domain' ),
		'add_new_item'        => __( 'Add New Item', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'edit_item'           => __( 'Edit Item', 'text_domain' ),
		'update_item'         => __( 'Update Item', 'text_domain' ),
		'search_items'        => __( 'Search Item', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'chart', 'text_domain' ),
		'description'         => __( 'Post Type Description', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'chart', $args );

}

// Hook into the 'init' action
add_action( 'init', 'chart', 0 );



function chartInputFields($count, $chart_id) {
	global $prefix;
	$i = 1;

	while ($i <= $count) {
		$field['artist'.$i]['name'] = "Number {$i}'s Artist Name";
		$field['artist'.$i]['id'] = "{$prefix}{$chart_id}_p{$i}artist"; 
		$field['artist'.$i]['desc'] = "What is the name of the Artist that is this week's Number {$i}?";
		$field['artist'.$i]['type'] = 'text';
		$field['artist'.$i]['clone'] = false;
		
		$field['track'.$i]['name'] = "Number {$i}'s Track Title";
		$field['track'.$i]['id'] = "{$prefix}{$chart_id}_p{$i}track";
		$field['track'.$i]['desc'] = "What is the Track Title?";
		$field['track'.$i]['type'] = "text";
		$field['track'.$i]['clone'] = false;
		
		$field['divider'.$i]['name'] = "Divider";
		$field['divider'.$i]['id'] = "{$prefix}{$chart_id}_p{$i}divider";
		$field['divider'.$i]['type'] = "divider";
		$i++;

	}
	return $field;
}

function displaySRAChart($chart_id) {
	$data = getSRAChart($chart_id);
//	var_dump($data);
	$output = "";
	foreach ($data as $key => $item) {
		$output .= "<div id=\"".$chart_id."_".$key."\" class=\"thumbnail\" data-mbid=\"".$item['mbid']."\">";
		$output .= "<div class=\"row\">";
		$output .= "<div class=\"the_image col-xs-4\">";
		$output .= "<img src=\"".$item['image']."\" class=\"img-responsive\" style=\"width:100%\" />";
		$output .= "</div>";
		$output .= "<div class=\"col-xs-8\">";
		$output .= "<h3>".$item['artist']."</h3>";
		$output .= "<p>".$item['track']."</p>";
		$output .= "</div>";
		$output .= "</div></div>";
	}
	return $output;
}
function getSRAChart($chart_id) {
	$allmeta = get_post_meta(get_the_ID());
	$id_length = strlen($chart_id);
	foreach ($allmeta as $key => $item) {
		if (substr($key, 9, $id_length) != $chart_id) {
			unset($allmeta[$key]);
		}
	}
	// IF THERE IS NOTHING LEFT THAT MATCHES THE CHART ID
	if (empty($allmeta))
		return;
		
	$full_length = 12+$id_length;
	
	foreach ($allmeta as $key => $item) {
		$position = substr($key, $id_length+11, 1);
		if (substr($key, $full_length) == "artist") {
			$chart[$position]['artist'] = $item[0];
			$brainz = decode_musicbrainz($item[0]);
			$chart[$position]['mbid'] = $brainz->artist->mbid;
			$chart[$position]['images'] = $brainz->artist->image;
			$chart[$position]['image'] = $brainz->artist->image{4}->{"#text"};

		} else {
			$chart[$position]['track'] = $item[0];
		}
	}
	// Returns an Array
	return $chart;
}

function decode_musicbrainz($artist) {
	$url = "http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=".urlencode($artist)."&api_key=f7c2a0ee6efde7d20f7387e2c254370e&format=json";
	$json = file_get_contents($url);
	$json = utf8_encode($json);
	$json = json_decode($json);
	
	return $json;
}
