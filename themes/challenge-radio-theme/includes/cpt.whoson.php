<?php
if ( ! function_exists('whoson') ) {

// Register Custom Post Type
function whoson() {

	$labels = array(
		'name'                => _x( 'Presenters', 'Post Type General Name', 'fredbradley' ),
		'singular_name'       => _x( 'Presenter', 'Post Type Singular Name', 'fredbradley' ),
		'menu_name'           => __( 'Presenters', 'fredbradley' ),
		'parent_item_colon'   => __( 'Parent Item:', 'fredbradley' ),
		'all_items'           => __( 'All Items', 'fredbradley' ),
		'view_item'           => __( 'View Item', 'fredbradley' ),
		'add_new_item'        => __( 'Add New Item', 'fredbradley' ),
		'add_new'             => __( 'Add New', 'fredbradley' ),
		'edit_item'           => __( 'Edit Item', 'fredbradley' ),
		'update_item'         => __( 'Update Item', 'fredbradley' ),
		'search_items'        => __( 'Search Item', 'fredbradley' ),
		'not_found'           => __( 'Not found', 'fredbradley' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'fredbradley' ),
	);
	$args = array(
		'label'               => __( 'presenters', 'fredbradley' ),
		'description'         => __( 'Who is On!?', 'fredbradley' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'presenters', $args );

}

// Hook into the 'init' action
add_action( 'init', 'whoson', 0 );

}