<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package challengeradio
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
<link rel="author" href="https://plus.google.com/110474516290880858606" /> <!-- https://plus.google.com/109314841734487245346/about -->
<link rel="publisher" href="https://plus.google.com/110474516290880858606" />

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44652794-3', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body <?php body_class(); ?>>
<script type="text/javascript">
jQuery( document ).ready(function($) {
	$('a.scroller').click(function(){
	    $('html, body').animate({
	        scrollTop: $( $(this).attr('href') ).offset().top
	    }, 500);
	    return false;
	});
	//$('#start-button').removeClass("hide");
	$('#start-button').fadeIn(2000).addClass("show-button");
	$('#start-button').click(function() {
		$(this).fadeOut(500);
	})
});
</script>
<?php
	$items = array(
		array(
			'class' => 'twitter',
			'content' => "1 My wonderful tweet",
		),
		array(
			'class' => 'facebook',
			'content' => '2 My Facebook is awesome!'
		),
		array(
			"content" => "3 Here is a star, shining bright!"
		),
		array(
			'class' => 'twitter',
			'content' => "4 My wonderful tweet",
		),
		array(
			'class' => 'facebook',
			'content' => '5 My Facebook is awesome!'
		),
		array(
			"content" => "6 Here is a star, shining bright!"
		)
	);


$ticker = newsTicker($items);
?>
	<?php do_action( 'before' ); ?>
	
<header id="masthead" class="site-header" role="banner">
	<div class="container">
		

	
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2 text-center">
				<a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="content" class="scroller">
					<h1 class="sr-only"><?php bloginfo('name'); ?></h1>
					<img id="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/ChallengeRadioLogoTransparent-WhiteText.png" class="img-responsive blink" />
				</a>
			</div>
		</div>
		
	</div><!-- .container -->
	<div class="push-down-header">&nbsp;</div>
</header><!-- #masthead -->


<div class="main-content">
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner col-sm-12 col-md-12">

			
