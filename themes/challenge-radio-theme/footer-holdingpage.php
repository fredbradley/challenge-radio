<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package challengeradio
 */
?>
			</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->
<div id="flare-footer">&nbsp;</div>
<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="container">
		<!--<div class="row">
			<div class="col-md-9 col-sm-6">
			&nbsp;</div>
			
			<div class="col-md-3 col-sm-6">
				<img src="<?php echo get_stylesheet_directory_uri() .'/images/radfest_logo.png'; ?>" class="img-responsive" />
			</div>
			
			
		</div> -->
	</div>

	
</footer><!-- close #colophon -->
<script type="text/javascript" async src="//platform.twitter.com/widgets.js"></script>
<?php wp_footer(); ?>
<script>
jQuery( document ).ready(function($) {
  // Handler for .ready() called.
	var nt_example2 = $('#nt-example2').newsTicker({
		row_height: 30,
		max_rows: 1,
	//	speed: 300,
		duration: 8000,
		prevButton: $('#nt-example2-prev'),
		nextButton: $('#nt-example2-next'),
		hasMoved: function() {
	    	$('#nt-example2-infos-container').fadeOut(200, function(){
	        	$('#nt-example2-infos .infos-hour').text($('#nt-example2 li:first span').text());
	        	$('#nt-example2-infos .infos-text').text($('#nt-example2 li:first').data('infos'));
	        	$(this).fadeIn(400);
	        });
	    },
	    pause: function() {
	    	$('#nt-example2 li i').removeClass('fa-play').addClass('fa-pause');
	    },
	    unpause: function() {
	    	$('#nt-example2 li i').removeClass('fa-pause').addClass('fa-play');
	    }
	});
	$('#nt-example2-infos').hover(function() {
	    nt_example2.newsTicker('pause');
	}, function() {
	    nt_example2.newsTicker('unpause');
	});

	jQuery('#newsTicker ul').newsTicker({
	    row_height: 20,
	    max_rows: 1,
	    duration: 4000,
	//    prevButton: jQuery('#nt-example1-prev'),
	//    nextButton: jQuery('#nt-example1-next')
	});

	jQuery('#emapTweet').newsTicker({
		row_height:100,
		max_rows:1,
		duration:6000,
		direction: 'down',

	});
		jQuery('#emapTweet').append("<li>Follow Challenge Radio on Twitter... <a href='http://twitter.com/challengeradio'>@ChallengeRadio</a></li>");

});
</script>
</body>
</html>