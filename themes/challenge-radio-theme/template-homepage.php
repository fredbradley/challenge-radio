<?php
/**
 * Template Name: HomePage
 * The template for displaying the homepage!
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package challengeradio
 */
if (do_holdingpage_redirect()):
	get_header('holdingpage');
else:
	get_header();
endif;
 ?>
 <?php if (do_holdingpage_redirect() === false): ?>
	<div class="feature-blocks row">
		
		<!-- BIG NEWS -->
		<div class="col-sm-6">
			<div class="featuredItem">
			<?php 
			$args = array(
					'post_type' => all_post_types(),
					'posts_per_page' => 1,
				);query_posts( $args ); 
			while(have_posts()) : the_post();
				echo homepage_content_block();
			endwhile; wp_reset_query(); ?>
			</div>
		</div>
		
		<!-- FIRST FOUR -->
		<div class="col-sm-6">
			<div class="row">
			<?php 
				$args = array(
					'post_type' => all_post_types(),
					'posts_per_page' => 4,
					'offset' => 1
				);
			query_posts($args);
				while(have_posts()) : the_post(); ?>
				<div class="col-xs-6">
					<?php echo homepage_content_block(); ?>
				</div>
				<?php endwhile; wp_reset_query(); ?>
			</div>
		</div>
		
		<!-- SECOND FOUR -->
		<div class="col-sm-12 hidden-xs" id="extraBlocks">
			<div class="row">
			<?php 
				$args = array(
					'post_type' => all_post_types(),
					'posts_per_page' => 24,
					'offset' => 5
				);
			query_posts($args);
				while(have_posts()) : the_post(); ?>
				<div class="col-xs-3">
					<?php echo homepage_content_block(); ?>
				</div>
				<!--<div class="col-xs-3">
					<?php echo homepage_content_block(); ?>
				</div>
				<div class="col-xs-3">
					<?php echo homepage_content_block(); ?>
				</div>
				<div class="col-xs-3">
					<?php echo homepage_content_block(); ?>
				</div>
				<div class="col-xs-3">
					<?php echo homepage_content_block(); ?>
				</div>-->
				<?php endwhile; wp_reset_query(); ?>
			</div>
		</div>
		
		
		
		
	</div><!-- .feature-blocks -->
	
	<?php else: ?>
	
	<!-- NOTHING -->		
	<?php endif; ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'content', 'homepage' ); ?>
	<?php endwhile; // end of the loop. ?>

<?php 
	if (do_holdingpage_redirect() === false):
		get_sidebar();
	endif;
	
if (do_holdingpage_redirect()):
	get_footer('holdingpage');
else:
	get_footer();
endif;

?>