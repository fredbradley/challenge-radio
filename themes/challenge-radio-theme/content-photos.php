<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package challengeradio
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="page-header">
		<h1 class="page-title"><?php the_title(); ?></h1>
	</header><!-- .entry-header -->

	<div class="entry-content row">
		<?php
		
		$images = get_post_meta(get_the_ID(), 'challenge_meta_gallery_page');
		$more_images = get_post_meta(get_the_ID(), 'challenge_meta_gallery_page_again');
	//	$images = array_merge($image, $more_images);
		foreach ($images as $image) :
			echo "<div class=\"col-sm-4 photo-container\">";
			if (is_user_logged_in()) {
				echo "<a target=\"_blank\" href=\"http://www.challengeradio.co.uk/wp-admin/admin-ajax.php?action=pte_ajax&pte-action=iframe&pte-id=".$image."\">";
			}
			
			echo wp_get_attachment_image( $image, 'newsimg', array(),array('class'=>'img-responsive img-rounded') );
			if (is_user_logged_in()) {
				echo "</a>";
			}
			echo "</div>";
		endforeach;
		foreach ($more_images as $image) :
			echo "<div class=\"col-sm-4 photo-container\">";
			if (is_user_logged_in()) {
				echo "<a target=\"_blank\" href=\"http://www.challengeradio.co.uk/wp-admin/admin-ajax.php?action=pte_ajax&pte-action=iframe&pte-id=".$image."\">";
			}
			
			echo wp_get_attachment_image( $image, 'newsimg', array(),array('class'=>'img-responsive img-rounded') );
			if (is_user_logged_in()) {
				echo "</a>";
			}
			echo "</div>";
		endforeach;

		?>
	
		<div class="col-sm-4">
				<?php the_post_thumbnail('newsimg', array('class' => 'img-responsive img-rounded pull-right')); ?>
		</div>
		<div class="col-sm-8">
				<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'challengeradio' ),
				'after'  => '</div>',
			) );
		?>
				</div>

	</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', 'challengeradio' ), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->
