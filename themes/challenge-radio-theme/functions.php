<?php
/**
 * challengeradio functions and definitions
 *
 * @package challengeradio
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 750; /* pixels */

show_admin_bar(false);
if ( ! function_exists( 'challengeradio_setup' ) ) :
/**
 * Set up theme defaults and register support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function challengeradio_setup() {
	global $cap, $content_width;

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	if ( function_exists( 'add_theme_support' ) ) {

		/**
		 * Add default posts and comments RSS feed links to head
		*/
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Enable support for Post Thumbnails on posts and pages
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		*/
		add_theme_support( 'post-thumbnails' );

		/**
		 * Enable support for Post Formats
		*/
		add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

		/**
		 * Setup the WordPress core custom background feature.
		*/
		add_theme_support( 'custom-background', apply_filters( 'challengeradio_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );
		
		/**
		 * Image Sizes
		 */
		add_image_size("talent", 600, 300, true);
		add_image_size("newsimg", 600, 600, true);
		add_image_size("radiovis", 320, 240, true);


	}

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on challengeradio, use a find and replace
	 * to change 'challengeradio' to the name of your theme in all the template files
	*/
	load_theme_textdomain( 'challengeradio', get_template_directory() . '/languages' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	*/
	register_nav_menus( array(
		'primary'  => __( 'Main Navigation', 'challengeradio' ),
		'global' => __( 'Global Menu', 'challengeradio' ),
		'foot_social' => __("Footer Shows", "challengeradio"),
		'foot_shows' => __("Footer Shows", "challengeradio"),
		'foot_other' => __("Footer Other", "challengeradio"),
		'foot_watch' => __("Footer Watch", "challengeradio"),
	) );

	foreach (glob(get_template_directory()."/includes/widget.*.php") as $filename) {
		require_once($filename);
	}
	require_once(get_template_directory()."/cpt.schedule.php");
	require_once(get_template_directory(). "/includes/cpt.chart.php");
}
endif; // challengeradio_setup
add_action( 'after_setup_theme', 'challengeradio_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function challengeradio_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'challengeradio' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'challengeradio_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function challengeradio_scripts() {

	// load challengeradio styles
	wp_enqueue_style( 'challengeradio-style', get_stylesheet_uri() );
	wp_enqueue_style('googlefont', 'http://fonts.googleapis.com/css?family=Indie+Flower|Varela+Round');
	
	wp_enqueue_style( 'wpbootstrap-css', get_template_directory_uri() . '/includes/css/bootstrap-wp.css');
	wp_enqueue_style( 'challengeradio-style-main', get_template_directory_uri() . '/includes/css/style.css');

	// load bootstrap js
	wp_enqueue_script('challengeradio-bootstrapjs', get_template_directory_uri().'/includes/js/bootstrap.js', array('jquery'));
	wp_enqueue_script('scrollto', get_template_directory_uri().'/includes/js/jquery.scrollTo.min.js', array('jquery'));
	// load bootstrap wp js
	wp_enqueue_script('newsTicker', get_template_directory_uri() . '/includes/js/jquery-advanced-news-ticker/jquery.newsTicker.js', array('jquery'), false, true);
	
	wp_enqueue_script( 'challengeradio-bootstrapwp', get_template_directory_uri() . '/includes/js/bootstrap-wp.js', array('jquery') );
	wp_enqueue_script('searchjs', get_template_directory_uri().'/includes/js/search.js', array('challengeradio-bootstrapjs'), '', true);
	wp_enqueue_script('twitterFetch', get_template_directory_uri() .'/includes/js/twitterFetch.js', array('jquery'), false, true);
	wp_enqueue_script( 'challengeradio-skip-link-focus-fix', get_template_directory_uri() . '/includes/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'challengeradio-keyboard-image-navigation', get_template_directory_uri() . '/includes/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
	}

//wp_deregister_script( 'jquery' );

}
add_action( 'wp_enqueue_scripts', 'challengeradio_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/includes/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/includes/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/includes/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/includes/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/includes/jetpack.php';

/**
 * Load custom WordPress nav walker.
 */
require get_template_directory() . '/includes/bootstrap-wp-navwalker.php';




function curl_json_decode($url) {
	if(!function_exists("curl_init")) die("cURL extension is not installed");

	$ch=curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$r=curl_exec($ch);
	curl_close($ch);

	$arr = json_decode($r,false);
	return $arr;
}

function top40() {
	$json = curl_json_decode("http://ben-major.co.uk/labs/top40/api/singles/");
	echo "<pre>";var_dump($json);echo "</pre>";
}


function itunes_chart($limit=25) {
	$xml = simplexml_load_file("https://itunes.apple.com/gb/rss/topsongs/limit=".$limit."/explicit=true/xml");
	
//	$output = "<div>";
	$output = "<div class=\"col-md-12\">";
	foreach ($xml->entry as $entry) {
		$image = $entry->children('im', true)->image;
		$image = $image[2];
		$link = $entry->link->attributes();
		$link = $link->href;
		$preview = false;

		$preview = $entry->link[1];
		if ($preview != null) {
			$preview = $preview->attributes();
			//		debugme($preview);
			$preview = itunes_preview($preview);
		}
		$buy = $entry->children('im', true)->price;
		$price = $buy->attributes()->amount;
		if ($price < 0) {
			$buy = "";
		}

		
		$title = $entry->title;
		$updated = $entry->updated;
	
		$output .= "<div class=\"row\" data-updated=\"".$updated."\">";

		$output .= "<a href=\"".$link."\" target=\"_blank\">";
		$output .= "<div class=\"col-xs-4\">";
		$output .= "<img src=\"".$image."\" class=\"img-responsive\" />";
		$output .= "</div>";

		$output .= "<div class=\"col-xs-4\">";
		$output .= "<p>".$title."</p>";
		$output .= "</div>";
		
		$output .= "<div class=\"col-xs-4\">";
		$output .= "<span class=\"label label-primary\">".$buy."</span>";
		$output .= $preview;
		
		$output .= "</div>";

		$output .= "</a>";		
		$output .= "</div>";
	
//	$output .= $entry->content;			
	}
	
	$output .= "</div>";
	
	return $output;
}

function itunes_preview($input) {
		$output = "<span class=\"preview\" data-href=\"".$input->href."\" data-type=\"".$input->type."\"><i class=\"fa fa-fw fa-play\"></i></span>";
	
	return $output;
}

function create_chart_json() {
	$xml = "http://radiomonitor.com/api/ukairplaychart/?action=get_airplay_chart&country=uk&channel_type=radio&group=&channel_id=&";
	$xml = simplexml_load_file($xml);
	
	$title = $xml->chart_title;
	
	$i = 0;
	foreach ($xml->chart_entry as $entry) {
		/*$artist = "http://ws.audioscrobbler.com/2.0/?limit=1&method=artist.search&artist=".urlencode($entry->artist)."&api_key=f7c2a0ee6efde7d20f7387e2c254370e&format=json";
		$artist = curl_json_decode($artist);
		$artist_name = $artist->results->artistmatches->artist->name;
		$track = "http://ws.audioscrobbler.com/2.0/?limit=1&method=track.search&track=".urlencode($entry->title)."&artist=".urlencode($artist_name)."&api_key=f7c2a0ee6efde7d20f7387e2c254370e&format=json";
		$track = curl_json_decode($track);
		*/
		$se7en = simplexml_load_file("http://api.7digital.com/1.2/track/search?q=".$entry->title."&oauth_consumer_key=YOUR_KEY_HERE&country=GB&pagesize=2&imageSize=500");

		$mb_artist = "http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=".urlencode($entry->artist)."&api_key=f7c2a0ee6efde7d20f7387e2c254370e&format=json";
		$artist = curl_json_decode($mb_artist);
		$chart[$i]['track'] = (string)$entry->title;
		$chart[$i]['mbid'] = $artist->artist->mbid;
		$chart[$i]['artist'] = $artist->artist;
//		$chart[$i]['seven'] = $se7en->searchResults->searchResult;
		$chart[$i]['image'] = (string)$se7en->searchResults->searchResult->track->release->image;
	//	$chart[$i]['url'] = $track->results->trackmatches->track->url;
	//	$chart[$i]['image'] = $track->results->trackmatches->track->image;
		
		
		
		$i++;
		if ($i > 9) {
			break;
		}
	}
	$json = json_encode($chart);
	return $json;
}

function display_chart_json() {
	$json = json_decode(create_chart_json());
	
	
	$output = "<div class=\"\">";
	
	foreach ($json as $item) {
		$bbc = curl_json_decode("http://www.bbc.co.uk/music/artists/".$item->mbid.".json");
		$output .= "<div class=\"row\">";
		$image = $item->images[3];
		if ($image == null) {
			$image = "http://placehold.it/300";
		} else {
			$image = $image->{"#text"};
		}
		
		$link = $item->url;

		$output .= "<div class=\"col-md-3\"><a href=\"".$link."\" target=\"_blank\"><img src=\"".$image."\" class=\"img-responsive\" /></a></div>";
		//$output .= "<div class=\"col-md-3\">".$item->track."<br />".$item->artist."</div>";
		$output .= "<div class=\"col-md-6\">".$item->mbid."</div>";
		$output .= "</div>";
	}
	
	$output .= "</div>";
	
	
	return $json;
}

function debugme($data) {

	if (is_array($data) || is_object($data)) {
		echo "<pre>";
		var_dump($data);
		echo "</pre>";
	} else {
		return $data;
	}
	
}



function newsTicker($items) {
/*	<li><div class="twitter"><i class="fa fa-fw fa-twitter"></i>Here is a tweet!</div></li>
		<li><div class="facebook"><i class="fa fa-fw fa-facebook"></i>Here is a facebook post!</div></li>
		<li><div><i class="fa fa-fw fa-star"></i>Here is something different!</div></li>
*/
	$defaults = array(
		'class' => 'star',
		'content' => "",
	);

	foreach ($items as $item) {
		$args = wp_parse_args( $item, $defaults );
		$output .= "<li>";
		$output .= "<div class=\"".$args['class']."\">";
		$output .= "<i class=\"fa fa-fw fa-".$args['class']."\"></i>";
		$output .= $args['content'];
		$output .= "</div></li>";
	}
	return $output;
}


function xhomepage_content_block() { ?>
			<div class="cuadro_intro_hover">
				<a href="<?php the_permalink(); ?>">
					<p>
						<?php the_post_thumbnail("newsimg", array('class'=>"img-responsive")); ?>
					</p>
					<div class="caption">
						<div class="blur"></div>
						<div class="caption-text">
							<h3><?php the_title(); ?></h3>
							<p><?php the_excerpt(); ?></p>
							<!--<a class=" btn btn-default" href="#"><span class="glyphicon glyphicon-plus"> INFO</span></a> -->
						</div>
					</div>
				</a>
			</div>
<?php }

function homepage_content_block($blank=null) { ?>
				<div class="fblockcontainer">
					<div class="cuadro_intro_hover ">
						<p class="thumbnail_holder">
							<?php the_post_thumbnail("newsimg", array('class'=>'img-responsive ')); ?>
						</p>
						<div class="caption">
							<div class="blur"></div>
							<a href="<?php the_permalink(); ?>">
								<div class="caption-text">
									<h3><?php the_title(); ?></h3>
									<?php the_excerpt(); ?>
								</div>
							</a>
						</div>
					</div>
				</div>
<?php }


function compile_sass() {
	echo "<div class=\"header_buttons\"><p>";
	if (isset($_GET['compile_scss'])):
		$compile = shell_exec("compass compile ".get_template_directory());
		if ($compile):
			echo "<span class=\"button button-primary\"><i class=\"fa fa-check\"></i>Compiled!</span>";
		else:
			echo "<span class=\"button button-primary\"><i class=\"fa fa-compass\"></i>Nothing to compile.</span>";
		endif;
	else:
		echo "<a class=\"button button-primary\" href=\"".admin_url('index.php?compile_scss=true')."\"><i class=\"fa fa-terminal\"></i>Compile CSS</a>";
	endif;
	echo purge_debug_log();
	echo "</p></div>";
}
add_action('admin_notices', 'compile_sass', 100);


function purge_debug_log() {
	$file = ABSPATH."wp-content/debug.log";
	echo "<a href=\"".admin_url('index.php?purge_debug=true')."\" class=\"button\">Purge Debug Log</a>";
	if (isset($_GET['purge_debug'])):
		shell_exec("cat /dev/null > ".$file);
		echo "<div class=\"updated\"><p><strong>You're amazing!</strong><br /> The Debug Log has now been Purged! Eugh.. that's a sigh of relief!</p></div>";
	endif;
}

function log_me($message) {
    if (WP_DEBUG === true) {
        if (is_array($message) || is_object($message)) {
            error_log(print_r($message, true));
        } else {
            error_log($message);
        }
    }
}


function is_redirect_on() {
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	
	$plugin = 'challenge-radio-redirects/redirect.php';

	// check for plugin using plugin name
	if ( is_plugin_active( $plugin ) ) {
  //plugin is activated
  		return true;
  	} else {
		return false;
	}
}

function do_holdingpage_redirect() {
	if (!is_user_logged_in() && is_redirect_on()):
		return true;
	else:
		return false;
	endif;
}




function all_post_types() {
	return array('post', 'presenters', 'programmes', 'pages');
}

function time_ago($timestamp){
    //type cast, current time, difference in timestamps
    $timestamp      = (int) $timestamp;
    $current_time   = time();
    $diff           = $current_time - $timestamp;
    
    //intervals in seconds
    $intervals      = array (
        'year' => 31556926, 'month' => 2629744, 'week' => 604800, 'day' => 86400, 'hour' => 3600, 'minute'=> 60
    );
    
    //now we just find the difference
    if ($diff == 0)
    {
        return 'just now';
    }    

    if ($diff < 60)
    {
        return $diff == 1 ? $diff . ' second ago' : $diff . ' seconds ago';
    }        

    if ($diff >= 60 && $diff < $intervals['hour'])
    {
        $diff = floor($diff/$intervals['minute']);
        return $diff == 1 ? $diff . ' minute ago' : $diff . ' minutes ago';
    }        

    if ($diff >= $intervals['hour'] && $diff < $intervals['day'])
    {
        $diff = floor($diff/$intervals['hour']);
        return $diff == 1 ? $diff . ' hour ago' : $diff . ' hours ago';
    }    

    if ($diff >= $intervals['day'] && $diff < $intervals['week'])
    {
        $diff = floor($diff/$intervals['day']);
        return $diff == 1 ? $diff . ' day ago' : $diff . ' days ago';
    }    

    if ($diff >= $intervals['week'] && $diff < $intervals['month'])
    {
        $diff = floor($diff/$intervals['week']);
        return $diff == 1 ? $diff . ' week ago' : $diff . ' weeks ago';
    }    

    if ($diff >= $intervals['month'] && $diff < $intervals['year'])
    {
        $diff = floor($diff/$intervals['month']);
        return $diff == 1 ? $diff . ' month ago' : $diff . ' months ago';
    }    

    if ($diff >= $intervals['year'])
    {
        $diff = floor($diff/$intervals['year']);
        return $diff == 1 ? $diff . ' year ago' : $diff . ' years ago';
    }
}

add_filter('jetpack_open_graph_tags', 'changejetpacktags');
function changejetpacktags( $tags ){
 
	// TWITTER FIXES:
	// wpcom defaults to the 'photo' card type for all posts but it should be 'summary' or not used at all ('summary' is default if no card type is specified.)
	if( is_single() && $tags['twitter:card' ] === 'photo' )
		unset(  $tags['twitter:card' ] );
	// wpcom sets card author to @wordpresscom, you should set it to your site's twitter handle ( or the author's handle. )
	if( is_single() )
		$tags['twitter:creator']= "@ChallengeRadio";
 
	
 
	// OG META FIXES 
	// Jetpack doesn't provide a description for the homepage so use the blog's description
	// Jetpack uses 'article' for the site's home and 'website' is more appropriate.
	if( is_home() ){
		$tags['og:description'] = esc_attr( get_bloginfo( 'description' ) );
		$tags['og:type'] = 'website';
		$tags['og:image'] = "http://www.challengeradio.co.uk/wp-content/uploads/2014/10/ChallengeRadioLogo_Alt2_RGB-600x300.jpg?bbe57";
		$tags['twitter:image'] = "";
	}
 
	// FACEBOOK ARTICLE PUBLISHER TAG SUPPORT
	if( is_single() )
		$tags['article:publisher'] = 'https://www.facebook.com/ChallengeRadio';
	
	if (!$tags['og:image'])
		$tags['og:image'] = "http://www.challengeradio.co.uk/wp-content/uploads/2014/10/ChallengeRadioLogo_Alt2_RGB-600x300.jpg?bbe57";
	
	// return the modified open graph tags
	return $tags;
}


add_action( 'rwmb_meta_boxes', 'your_prefix_register_meta_boxes' );

function your_prefix_register_meta_boxes( $meta_boxes )
{
	$prefix = 'challenge_meta_';

	// 1st meta box
	
	
	
	$meta_boxes[] = array(
		'id'     => 'show_times',
		'title'  => __( 'Show Times', 'meta-box' ),
		'pages'  => array('programmes'),
		

		'fields' => array(
			array(
				'name' => "Short Name",
				'id' => $prefix . 'short_name',
				'type' => 'text',
			),
			array(
				'name' => "About This Show",
				'id' => $prefix . 'short_description',
				'type' => 'textarea'
			),
			array(
				'name' => __( 'Start Time', 'meta-box' ),
				'id'   => $prefix . 'start_time',
				'type' => 'time',
				// jQuery datetime picker options. See here http://trentrichardson.com/examples/timepicker/
				'js_options' => array(
					'stepMinute' => 30,
					'showSecond' => false,
					'stepSecond' => 10,
				),
			),
			array(
				'name' => __( 'End Time', 'meta-box' ),
				'id'   => $prefix . 'end_time',
				'type' => 'time',
				// jQuery datetime picker options. See here http://trentrichardson.com/examples/timepicker/
				'js_options' => array(
					'stepMinute' => 30,
					'showSecond' => false,
					'stepSecond' => 10,
				),
			),
		)
	);

	// Other meta boxes go here

	$meta_boxes[] = array(
		'id' => 'challenge_photos_gallery', 
		'title' => "Gallery",
		'pages' => array('page', 'post'),
		
		'fields' => array(
			array(
				'id' => $prefix . 'gallery_page',
				'name' => "Photos",
				'type'             => 'image_advanced',
				'max_file_uploads' => 25,
			),
			array(
				'id' => $prefix . 'gallery_page_again',
				'name' => "More Photos",
				'type'             => 'image_advanced',
				'max_file_uploads' => 25,
			)
		),
	);


$meta_boxes[] = array(
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'date_of_chart',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => __( 'Chart Date', 'sra' ),

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array('chart'),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'side',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',

	// Auto save: true, false (default). Optional.
	'autosave' => true,

	// Fred has written a clever function to help us with this! :) Isn't he nice!
	'fields' => array (
		array(
		'name' => 'Artist',
		'id' => "{$prefix}artist",
		'type' => 'text'
		),
		array(
		'name' => 'Title',
		'id' => "{$prefix}title",
		'type' => 'text'
		),
		array(
			'name' => 'number',
			'id' => "{$prefix}number",
			'type' => 'number'
		)
	),
);

$prefix = 'srachart_';


$meta_boxes[] = array(
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'date_of_chart',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => __( 'Chart Date', 'sra' ),

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array('sra_chart'),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'side',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',

	// Auto save: true, false (default). Optional.
	'autosave' => true,

	// Fred has written a clever function to help us with this! :) Isn't he nice!
	'fields' => array (
		array(
		'name' => 'Date of Chart',
		'id' => "{$prefix}chartDate",
		'type' => 'date'
		),
	),
);
// 1st meta box
$meta_boxes[] = array(
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'commercial_chart_positions',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => __( 'This Week\'s Commercial Chart', 'sra' ),

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array('sra_chart'),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',

	// Auto save: true, false (default). Optional.
	'autosave' => true,

	// Fred has written a clever function to help us with this! :) Isn't he nice!
	'fields' => chartInputFields(10, 'commercial'),
);
// 2nd Meta Box
$meta_boxes[] = array(
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'unsigned_chart_positions',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => __( 'This Week\'s Unsigned Chart', 'sra' ),

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array('sra_chart'),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',

	// Auto save: true, false (default). Optional.
	'autosave' => true,

	// Fred has written a clever function to help us with this! :) Isn't he nice!
	'fields' => chartInputFields(10, 'unsigned'),
);





	return $meta_boxes;
}