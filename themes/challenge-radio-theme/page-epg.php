<?php
	date_default_timezone_set("Europe/London");

function get_the_time_of_show($meta) {
	$timestamp = strtotime("2014-10-14 ".$meta);
	return $timestamp;
}


$array = array(76,78,80,82,84,86,88,90,92,94);
	$i = 0;
foreach ($array as $station) {
	$post = get_post($station);
	
	$epg[$i]['id'] = $post->ID;
	$start_time = get_the_time_of_show(get_post_meta($post->ID, 'challenge_meta_start_time', true));
	$end_time = get_the_time_of_show(get_post_meta($post->ID, 'challenge_meta_end_time', true));
	$epg[$i]['start'] = $start_time;
	$epg[$i]['end'] = $end_time;
	$shortname = get_post_meta($post->ID, 'challenge_meta_short_name', true);
	if (empty($shortname)) {
		$epg[$i]['title'] = $post->post_title;
	} else {
		$epg[$i]['title'] = get_post_meta($post->ID, 'challenge_meta_short_name', true);
	}
	$description = get_post_meta($post->ID, 'challenge_meta_short_description', true);
	if (empty($description)) {
		$epg[$i]['description'] = "<p>Join ".$epg[$i]['title']." live from the dome until ".date('g:i a', $epg[$i]['end']).".";
	} else {
		$epg[$i]['description'] = $description;
	}
	$url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' ); 
	$epg[$i]['image'] = $url[0];
	
	$i++;
	
}

$json = json_encode($epg);
header('Content-Type: application/json');
echo $json;

