<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package challengeradio
 */

get_header(); ?>

	<?php // add the class "panel" below here to wrap the content-padder in Bootstrap style ;) ?>
	<div class="content-padder">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title">
					Presenters
				</h1>
				<?php
					// Show an optional term description.
					$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );
					endif;
				?>
			</header><!-- .page-header -->
			<div class="entry-summary">
<!--				<p>Join us from 8am on Tuesday 14th October to hear Challenge Radio live.</p>-->
			</div>

			<?php /* Start the Loop */ 
			
			$args = array(
				'posts_per_page' => -1,
				'post_type' => 'presenters',
				'orderby' => 'title',
				'order' => 'ASC'
			);
			query_posts($args);
			
			?>
			<div class="feature-blocks row">
		
				<?php 
					query_posts($args);
					while(have_posts()) : the_post(); ?>
					<!-- FIRST FOUR -->
				<div class="col-sm-4 col-xs-6">
					<?php echo homepage_content_block(); ?>
				</div>
		
				<?php endwhile; wp_reset_query(); ?>

			</div><!-- .feature-blocks -->


		<?php else : ?>

			<?php get_template_part( 'no-results', 'archive' ); ?>

		<?php endif; ?>

	</div><!-- .content-padder -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
<?php 

function get_time_of_show($meta) {
	$timestamp = strtotime("2014-10-14 ".$meta);
	return $timestamp;
}

function display_show_times($id) {
	$start_time = get_time_of_show(get_post_meta($id, 'challenge_meta_start_time', true));
	$end_time = get_time_of_show(get_post_meta($id, 'challenge_meta_end_time', true));
	
	$start_date = date("g:i a", $start_time);
	$end_date = date("g:i a", $end_time);
	
	return $start_date." - ".$end_date;
}