<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package sra2k13
 * @since sra2k13 1.0
 */

get_header(); ?>

	<div class="row">
		<div id="primary" class="col-md-8 content-area">
			<div id="content" class="site-content" role="main">
			<h1><?php the_title(); ?></h1>

				<?php echo displaySRAChart('commercial'); ?>
			
			</div><!-- #content .site-content -->
		</div><!-- #primary .content-area -->
		<div class="col-md-4" id="sidebar">
			<div class="widget the_chart_widget">
				<h3>The Chart</h3>
			</div>
		</div>
	</div><!-- .row -->
<?php get_footer(); ?>